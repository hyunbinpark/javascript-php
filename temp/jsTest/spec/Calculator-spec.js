//import calc1 from '/Users/hyun/work/temp/jsTest/spec/cal_EngineTest_es6.js';
//var calc1 = require('/Users/hyun/work/temp/jsTest/spec/cal_EngineTest_es6.js');
var calc1 = require('/Users/hyun/work/temp/jsTest/spec/cal_EngineTest_es5.js');
describe("add success test", function () {
  it("should add 3 and 4", function() {
    let Calc1 = new calc1();

    let answer = Calc1.add(3,4);
    expect(answer).toEqual(7);
  });
});

describe("add bignumber test", function () {
  it("should add 1564895 and 765486468", function() {
    var answer = calc1.add(1564895, 765486468)
    expect(answer).toEqual(767051363);
  });
});

describe("throws an error when parameter is not a number", function () {
  it("should add apple and 546", function() {
    var answer = calc1.add("apple", 546);
    expect(function (){
      throw new Error("Check the Parameter")
    }).toThrow(new Error("Check the Parameter"));
  });
});

//-----------------------------------add Test ----------------------------------------//



describe("del success test", function () {
  it("should del 5 and 4", function() {
    let Calc1 = new calc1();

    let answer = Calc1.del(4,3);
    expect(answer).toEqual(1);
  });
});

describe("del bignumber test", function () {
  it("should del 9999999 and 8888888", function() {
    var answer = calc1.del(9999999, 8888888)
    expect(answer).toEqual(1111111);
  });
});

describe("throws an error when parameter is not a number", function () {
  it("should del apple and 546", function() {
    var answer = calc1.del("apple", 546);
    expect(function (){
      throw new Error("Check the Parameter")
    }).toThrow(new Error("Check the Parameter"));
  });
});

//-----------------------------------del Test ----------------------------------------//



describe("multple success test", function () {
  it("should multiple 5 and 4", function() {
    var answer = calc1.mult(5, 4);
    expect(answer).toEqual(20);
  });
});

describe("multple bignumber test", function () {
  it("should multiple 9999999 and 8888888", function() {
    var answer = calc1.mult(9999999, 11111);
    expect(answer).toEqual(111109988889);
  });
});

describe("throws an error when parameter is not a number", function () {
  it("should multiple apple and 546", function() {
    var answer = calc1.mult("apple", 546);
    expect(function (){
      throw new Error("Check the Parameter")
    }).toThrow(new Error("Check the Parameter"));
  });
});

//-----------------------------------mult Test ----------------------------------------//



describe("devide success test", function () {
  it("should devide 10 and 2", function() {
    var answer = calc1.devi(10, 2);
    expect(answer).toEqual(5);
  });
});

describe("devide bignumber test", function () {
  it("should devide 9999999 and 8888888", function() {
    var answer = calc1.devi(9999999, 1111111);
    expect(answer).toEqual(9);
  });
});

describe("throws an error when parameter is not a number", function () {
  it("should devide 5 and 0", function() {
    var answer = calc1.devi(5, 0);
    expect(function (){
      throw new Error("Check the Parameter")
    }).toThrow(new Error("Check the Parameter"));
  });
});
//-----------------------------------devi Test ----------------------------------------//
